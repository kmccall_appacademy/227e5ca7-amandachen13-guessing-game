# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

def guessing_game

  number = rand(1..100)
  guess_count = 0
  game = 'play'

  while game == 'play'
    puts "guess a number"
    guess = gets.chomp.to_i
    guess_count += 1
    puts guess
    if guess < number
      puts "too low"
    elsif guess > number
      puts "too high"
    else
      game = 'over'
      puts number
      puts guess_count
    end
  end

end

  # * Write a program that prompts the user for a file name, reads that file,
  #   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
  #   could create a random number using the Random class, or you could use the
  #   `shuffle` method in array.

def shuffle_file(file_name)
  lines = File.readlines(file_name)
  shuffled_lines = lines.shuffle

  File.open("#{file_name}-shuffled.txt", "w") do |file|
    shuffled_lines.each do |line|
      puts line
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length == 1
    shuffle_file(ARGV.shift)
  else
    puts "ENTER FILENAME TO SHUFFLE:"
    file_name = gets.chomp
    shuffle_file(file_name)
  end
end
